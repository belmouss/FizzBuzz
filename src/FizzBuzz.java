
public class FizzBuzz {

	public static int fizzBuzz(int fizz, int buzz, int test) {
		if (test % fizz == 0) {
			return fizz;
		} else if (test % buzz == 0) {
			return buzz;
		} else {
			return test;
		}
	}

}
